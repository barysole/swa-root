# SWA - Root

## Diagram of app architecture

![](Documentation/Application_Architecture.png)

## Description
This is the root of a system which implements basic library functions. System consist of several microservices:

Catalog service - https://gitlab.fel.cvut.cz/barysole/swa-catalog-service

User service - https://gitlab.fel.cvut.cz/barysole/swa-user-service

Borrow-return service - https://gitlab.fel.cvut.cz/barysole/swa-borrow-return-service

Service register - https://gitlab.fel.cvut.cz/barysole/swa-service-registry

Every microservice has their own repository with detailed separate descriptions. The services are build, test and create a new docker image after committing to the master branch. Every docker image that was created by this process pushes to docker-hub. This repository contains a docker-compose file by which you can run the whole system. Also the repository contains springBoot applications with integration tests.

## Use-cases

At this moment there was implemented following use-cases:

<sub><sup>Requirements: Someone needs to fill information about book by using book-catalog-service API methods.</sup></sub>

*1. Borrow book*
1. User register themselves into user-service. If user is already registered then go to point "2".
2. User send their credentials and information about the desired book to the borrow-return service.
3. Borrow-return service verifies if the book is available and credentials are valid.
4. If information from the request is correct then service changes book status in book-catalog-service and register operation.
5. User gets a response with "200" status.

*2. Reserve book*
1. User register themselves into user-service. If the user is already registered then go to point "2".
2. User send their credential and information about the book which is needed to be reserved to the borrow-return service.
3. Borrow-return service verifies if the book is available and credentials are valid.
4. If information from the request is correct then service changes book status in book-catalog-service and register operation.
5. User gets a response with "200" status.

*3. Return book*
1. Users borrow books.
2. User send their credentials and information about the returned book to the borrow-return service.
3. Borrow-return service verifies if a book exists and credentials are valid.
4. If information from the request is correct then the service changes book status in book-catalog-service and register operation.
5. User gets a response with "200" status.

*4. Remove reservation book*
1. User reserve book.
2. Users send their credentials and information about the reserved book to the borrow-return service.
3. Borrow-return service verify if book exists and reserved by user, check credential.
4. If information from the request is correct then service changes book status in book-catalog-service and register operation.
5. User gets a response with "200" status.

## Documentation
REST API documentation defined using Open API standard (Swagger) and implemented by using springdoc-openapi-ui library, described with microservices controller annotations and available by following link:
```
http://*SERVICE_HOST_NAME*:*SERVICE_PORT*/swagger-ui/index.html 
```
## How to run microservices
All build and run configurations are described in docker-compose.yml file, so you can simply run the project on your machine. Just install docker to your system and run the following command in the root directory.
```
docker-compose up
```
Docker will download images of microservices from docker-hub, build and run it by certain order. You can check current microservices state (health status implemented by using spring actuator) by command:
```
docker ps --no-trunc
```

## How to run integration tests
Integration tests will run automatically after every commit to this repo in the main branch. Also you can run them manually in GitLab. Integration test configuration described in .gitlab-ci.yml

## Microservices containers names and ports
- book-catalog-service -- 8235
- book-catalog-db-postgres -- not_bound
- user-service -- 8236
- user-db-postgres -- not_bound
- borrow-return-service -- 8234
- borrow-return-db-postgres -- not_bound
- register-service -- 8761

## Service register
Microservices use separate service registers (eureka) to detect themselves without host numbers. You can use http://localhost:8761 to view the Eureka dashboard.

## Semestral work tasks

1. Codebase – one codebase tracked in revision control, many deploys
   1. [x] separate repositories for each microservice (1)
   2. [x] continuous integration to build, test and create the artifact (3)
   3. [x] implement some tests and test each service separately (unit tests, integration tests) (5)
2. Dependencies – explicitly declare and isolate dependencies
   1. [x] preferably Maven project with pom.xml
   2. [ ] eventually gradle project or other
3. Config
   1. [x] configuration of services provided via environmental properties (1)
   2. [ ] eventually as configuration as code (bonus: 0.5)
4. Backing services – treat backing services as attached resources
   1. [x] backing services like database and similar will be deployed as containers too (1)
5. Build, release, run – strictly separate build and run stages
   1. [x] CI & docker
   2. [x] eventually upload your images to docker hub (bonus: 1)
6. [x] Processes – execute the app as one or more stateless processes (1)
7. [x] Port binding – export services via port binding (1)
8. Disposability – maximize robustness with fast startup and graceful shutdown
   1. [x] ability to stop/restart service without catastrophic failure for the rest (2)
9. Dev/prod parity – keep development, staging, and production as similar as possible
   1. [x] 3-5 integration tests (5)
   2. [x] services will be deployed as containers (2)
10. Logs – treat logs as event streams
    1. [x] log into standard output (1)
    2. [ ] eventually collect logs in Elastic (bonus: 0.5)
11. Communication
    1. [x] REST API defined using Open API standard (Swagger) (2)
    2. [x] auto-generated in each service (1)
    3. [x] clear URLs (2)
    4. [x] clean usage of HTTP statuses (2)
    5. [ ] eventually message based asynchronous communication via queue (bonus: 1)
12. Transparency – the client should never know the exact location of a service.
    1. [x] service discovery (2)
    2. [ ] eventually client side load balancing (bonus: 0.5) or workload balancing (bonus: 0.5)
13. Health monitoring – a microservice should communicate its health
    1. [x] Actuators (1)
    2. [ ] eventually Elastic APM (bonus: 1)
14. [x] Design patterns – use the appropriate patterns (2)
15. [x] Scope – use domain driven design or similar to design the microservices (5)
16. [x] Documentation – visually communicate architecture of your system (5)
    1. https://c4model.com/ (https://github.com/RicardoNiepel/C4-PlantUML)
