package cz.cvut.fel.barysole.client;

import cz.cvut.fel.barysole.pojo.dto.UserDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient(name = "UserServiceClient", url = "http://docker:8236")
public interface UserServiceClient {

    @RequestMapping(value = "/user/saveAll", method = RequestMethod.PUT)
    List<UserDTO> saveAllUsers(@RequestBody List<UserDTO> userList);

    @RequestMapping(value = "/user/save", method = RequestMethod.PUT)
    UserDTO saveUser(@RequestBody UserDTO user);

}
