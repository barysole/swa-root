package cz.cvut.fel.barysole.enums;

public enum BookManipulationOperation {
    RESERVED_BY_USER,
    BORROWED_BY_USER,
    RETURNED_BY_USER,
    RESERVATION_REMOVED_BY_USER
}
