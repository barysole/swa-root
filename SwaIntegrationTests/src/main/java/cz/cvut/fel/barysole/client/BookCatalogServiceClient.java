package cz.cvut.fel.barysole.client;

import cz.cvut.fel.barysole.pojo.dto.BookDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient(name = "BookCatalogServiceClient", url = "http://docker:8235")
public interface BookCatalogServiceClient {

    @RequestMapping(value = "/bookCatalog/saveAll", method = RequestMethod.PUT)
    List<BookDTO> saveAllBooks(@RequestBody List<BookDTO> bookList);

    @RequestMapping(value = "/bookCatalog/save", method = RequestMethod.PUT)
    BookDTO saveBook(@RequestBody BookDTO book);

}
