package cz.cvut.fel.barysole.pojo;

import lombok.Data;

@Data
public class BookAndUserPair {

    private String userId;

    private String bookId;

}
