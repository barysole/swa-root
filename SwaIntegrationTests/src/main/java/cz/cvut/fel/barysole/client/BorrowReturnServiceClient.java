package cz.cvut.fel.barysole.client;

import cz.cvut.fel.barysole.pojo.BookAndUserPair;
import cz.cvut.fel.barysole.pojo.dto.BookDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "BorrowReturnServiceClient", url = "http://docker:8234")
public interface BorrowReturnServiceClient {

    @RequestMapping(value = "/bookManipulation/borrow", method = RequestMethod.POST)
    BookDTO borrowBook(@RequestBody BookAndUserPair bookAndUserPair);

    @RequestMapping(value = "/bookManipulation/return", method = RequestMethod.POST)
    BookDTO returnBook(@RequestBody BookAndUserPair bookAndUserPair);

    @RequestMapping(value = "/bookManipulation/reserve", method = RequestMethod.POST)
    BookDTO reserveBook(@RequestBody BookAndUserPair bookAndUserPair);

    @RequestMapping(value = "/bookManipulation/removeReservation", method = RequestMethod.POST)
    BookDTO removeReservation(@RequestBody BookAndUserPair bookAndUserPair);

}
