package cz.cvut.fel.barysole.pojo.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import cz.cvut.fel.barysole.enums.BookManipulationOperation;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BookManipulationInformation {

    private String id;

    private String bookId;

    private String userId;

    private BookManipulationOperation operation;

}
