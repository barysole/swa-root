package cz.cvut.fel.barysole;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import cz.cvut.fel.barysole.client.BookCatalogServiceClient;
import cz.cvut.fel.barysole.client.BorrowReturnServiceClient;
import cz.cvut.fel.barysole.client.UserServiceClient;
import cz.cvut.fel.barysole.pojo.BookAndUserPair;
import cz.cvut.fel.barysole.pojo.dto.BookDTO;
import cz.cvut.fel.barysole.pojo.dto.UserDTO;
import feign.FeignException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = SwaIntegrationTestsApplication.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class SwaIntegrationTestsApplicationTests {

    @Autowired
    private BookCatalogServiceClient bookCatalogServiceClient;

    @Autowired
    private UserServiceClient userServiceClient;

    @Autowired
    private BorrowReturnServiceClient borrowReturnServiceClient;

    @Autowired
    private ResourceLoader resourceLoader;

    private static final Type BOOK_DTO_TYPE = new TypeToken<List<BookDTO>>() {
    }.getType();
    private static final Type USER_DTO_TYPE = new TypeToken<List<UserDTO>>() {
    }.getType();

    @Test
    @Order(1)
    public void request_of_books_creating_should_execute_successfully() throws IOException {
        List<BookDTO> bookList = retrieveBookTestSetFromFile();
        Assertions.assertDoesNotThrow(() -> bookCatalogServiceClient.saveAllBooks(bookList));
    }

    @Test
    @Order(2)
    public void request_of_users_creating_should_execute_successfully() throws IOException {
        List<UserDTO> userList = retrieveUserTestSetFromFile();
        Assertions.assertDoesNotThrow(() -> userServiceClient.saveAllUsers(userList));
    }

    @Test
    @Order(3)
    public void borrow_and_return_book_request_should_execute_successfully() {
        UserDTO testUser = generateTestUser();
        BookDTO testBook = generateBook();
        testUser = userServiceClient.saveUser(testUser);
        testBook = bookCatalogServiceClient.saveBook(testBook);
        BookAndUserPair bookAndUserPair = new BookAndUserPair();
        bookAndUserPair.setBookId(testBook.getId());
        bookAndUserPair.setUserId(testUser.getId());
        Assertions.assertDoesNotThrow(() -> borrowReturnServiceClient.borrowBook(bookAndUserPair));
    }

    @Test
    @Order(4)
    public void borrow_book_after_reservation_should_execute_successfully() {
        UserDTO testUser = generateTestUser();
        BookDTO testBook = generateBook();
        testUser = userServiceClient.saveUser(testUser);
        testBook = bookCatalogServiceClient.saveBook(testBook);
        BookAndUserPair bookAndUserPair = new BookAndUserPair();
        bookAndUserPair.setBookId(testBook.getId());
        bookAndUserPair.setUserId(testUser.getId());
        Assertions.assertDoesNotThrow(() -> borrowReturnServiceClient.reserveBook(bookAndUserPair));
        Assertions.assertDoesNotThrow(() -> borrowReturnServiceClient.borrowBook(bookAndUserPair));
    }

    @Test
    @Order(5)
    public void request_with_nonexistent_user_should_cause_exception() {
        BookDTO testBook = generateBook();
        testBook = bookCatalogServiceClient.saveBook(testBook);
        BookAndUserPair bookAndUserPair = new BookAndUserPair();
        bookAndUserPair.setBookId(testBook.getId());
        bookAndUserPair.setUserId("123aaa");
        Assertions.assertThrows(FeignException.NotFound.class, () -> borrowReturnServiceClient.reserveBook(bookAndUserPair));
        Assertions.assertThrows(FeignException.NotFound.class, () -> borrowReturnServiceClient.borrowBook(bookAndUserPair));
    }

    @Test
    @Order(6)
    public void request_with_nonexistent_book_should_cause_exception() {
        UserDTO testUser = generateTestUser();
        testUser = userServiceClient.saveUser(testUser);
        BookAndUserPair bookAndUserPair = new BookAndUserPair();
        bookAndUserPair.setBookId("123aaa");
        bookAndUserPair.setUserId(testUser.getId());
        Assertions.assertThrows(FeignException.NotFound.class, () -> borrowReturnServiceClient.reserveBook(bookAndUserPair));
        Assertions.assertThrows(FeignException.NotFound.class, () -> borrowReturnServiceClient.borrowBook(bookAndUserPair));
    }

    @Test
    @Order(7)
    public void attempt_to_borrow_reserved_book_should_cause_exception() {
        UserDTO testUser = generateTestUser();
        BookDTO testBook = generateBook();
        UserDTO testUser2 = generateTestUser();
        testUser = userServiceClient.saveUser(testUser);
        testUser2 = userServiceClient.saveUser(testUser2);
        testBook = bookCatalogServiceClient.saveBook(testBook);
        BookAndUserPair bookAndUserPair = new BookAndUserPair();
        bookAndUserPair.setBookId(testBook.getId());
        bookAndUserPair.setUserId(testUser.getId());
        Assertions.assertDoesNotThrow(() -> borrowReturnServiceClient.reserveBook(bookAndUserPair));
        bookAndUserPair.setUserId(testUser2.getId());
        Assertions.assertThrows(FeignException.BadRequest.class, () -> borrowReturnServiceClient.borrowBook(bookAndUserPair));
    }

    private List<BookDTO> retrieveBookTestSetFromFile() throws IOException {
        Resource resource = resourceLoader.getResource("classpath:testBooks.json");
        File inputFile = resource.getFile();
        List<BookDTO> data;
        Gson gson = new Gson();
        JsonReader reader = new JsonReader(new FileReader(inputFile));
        data = gson.fromJson(reader, BOOK_DTO_TYPE);
        return data;
    }

    private List<UserDTO> retrieveUserTestSetFromFile() throws IOException {
        Resource resource = resourceLoader.getResource("classpath:testUsers.json");
        File inputFile = resource.getFile();
        List<UserDTO> data;
        Gson gson = new Gson();
        JsonReader reader = new JsonReader(new FileReader(inputFile));
        data = gson.fromJson(reader, USER_DTO_TYPE);
        return data;
    }

    private UserDTO generateTestUser() {
        UserDTO userDTO = new UserDTO();
        userDTO.setLogin("SuperTestUser" + System.nanoTime());
        userDTO.setPassword("32f84eca-c036-437c-819f-ea9b2dbe14fe");
        userDTO.setEmail("superTestUser" + System.nanoTime() + "@gmail.com");
        userDTO.setFirstName("Firstname");
        userDTO.setMiddleName("Secondname");
        userDTO.setTelephone("+4 (988) 596-2881");
        return userDTO;
    }

    private BookDTO generateBook() {
        BookDTO bookDTO = new BookDTO();
        bookDTO.setAuthor("Logan Washington");
        bookDTO.setTitle("Some epic title - " + System.nanoTime());
        return bookDTO;
    }

}
